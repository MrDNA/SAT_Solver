import sys,re,logging,threading,time
from copy import deepcopy

"""
SAT Solver

besteht aus einer parse-Funktion zum parsen von cnf und einer solve-Funktion zum Finden einer Lösung zum geparsten Problem,
sowie den Klassen, für die Repräsentation des Problem verwendet werden.


"""

class Disjunction():
    #Repräsentiert eine Disjunktion

    #elements sind die Variablen die hier disjunkt verbunden sind
    def __init__(self,elements):
        self.elements=elements
    
    #true, wenn nur noch ein Element übrig ist und so zur Unit Propagation verwendet werden kann
    def is_unit(self):
        return len(self.elements)==1

    #aufrufen, wenn die Belegung einer Variable festgelegt wird
    def set(self,x):
        found=False
        for e in self.elements:#suchen nach der Varible in dieser Disjunktion
            if abs(e)==abs(x): #abs, damit auch negierte Variablen gefunden werden
                found=e
                break
        if found==False:#falls nichts gefunden wurde, 0 zurückgeben 
            return 0
        elif found==x:  #falls das gefundene Element das selbe Vorzeichen hat, ist die Disjunktion erfüllt und 1 wird zurückgegeben
            return 1
        else:           #falls das gefundene Element ein anderes Vorzeichen hat, kann die Disjunktion nicht mehr durch dieses Element erfüllt werden und es wird entfernt
            self.elements.remove(-x)
            return -1

    #legt die Repräsentation dieser Klasse als String fest
    def __repr__(self):
        return str(self.elements)

class UnsolvableException(Exception):#Exception, die geworfen wird, wenn durch das Setzen einer Variable eine Disjunktion unerfüllbar wird
    pass

class Problem():
    #Repräsentiert das ganze Problem, also die Konjunktion der Disjunktionen, von dem gezeigt werden soll das es lösbar ist

    def __init__(self, disjunktions):
        self.disjunctions=disjunktions #die Disjunktionen, die in dieser Kunjunktion verbunden sind
        self.interpretation=[]         #die Belegung der Variablen bisher

    #setzt eine Variable in allen Disjunktionen
    def set(self,var):
        satisfied=[]#Liste mit den Indices der erfüllten Disjunktionen 
        for i in range(len(self.disjunctions)):
            r=self.disjunctions[i].set(var)#in jeder Disjunktion setzen
            if r==1:#1 bedeutet die Disjunktion wurde erfüllt..
                satisfied.append(i)#..und kann der Liste hinzugefügt werden
            elif len(self.disjunctions[i].elements)==0:#falls es keine Elemente in der Disjunktion mehr gibt und nicht sie noch nicht erfüllt wurde, kann sie nicht mehr erfüllt werden
                raise UnsolvableException()
        #Löschen der bereits erfüllten Disjunktionen  
        c=0 #counter für gelöschte Elemente
        for i in satisfied: 
            del self.disjunctions[i-c]#index ist i-c, denn mit jedem gelöschten Elemente rücken die übrigen Elemente in der Liste auf und deren Index wird somit kleiner
            c+=1
        self.interpretation.append(var)#gesetzte Variable zur Nachverfolgung sichern

    #liefert das nächstbeste Element, das noch nicht belegt ist und eine Kopie dieses Problem-Objekts als Backtrackpunkt
    def split(self):    
        vars={}
        for d in self.disjunctions:
            for e in d.elements:
                var=str(abs(e))#abs zur Vergleichbarkeit und str um als key benutzt werden zu können 
                if var in vars:
                    vars[var]+=1 if e>0 else -1
                else:
                    vars[var]=1 if e>0 else -1 #falls es noch keinen Eintrag gibt wird einer mit entsprechendem Vorzeichen angelegt
        max=0
        e=0
        for var,count in vars.items():
            if abs(count)>=abs(max):
                e=int(var)
                max=count
        r=e if max>0 else -e
        if r==0:
            r=self.disjunctions[0].elements[0]
        return r,deepcopy(self)
      
    #sucht nach einer Disjunktion mit nur einem Element und gibt dieses Element zurück, falls eins gefunden wird
    def get_unit(self):
        for d in self.disjunctions:
            if d.is_unit():
                return d.elements[0]
        return None #None falls nichts gefunden wurde

    #sucht nach einer Variable, die in allen Disjunktionen, die das gleiche Vorzeichen hat, und gibt sie zuürck, falls eine gefunden wird
    def get_literal(self):
        literals={}#dict enthält die Variable als Key und als Value 1 für überall positive, -1 für überall negative und 0 für wechselnde Vorzeichen 
        for d in self.disjunctions:#iterieren über alle Disjunktionen..
            for e in d.elements:#..und alle ihre Elemente
                var=str(abs(e))#abs zur Vergleichbarkeit und str um als key benutzt werden zu können 
                if var in literals:
                    if not ((literals[var]==1 and e>0) or (literals[var]==-1 and e<0)):#falls das Vorzeichem hier anders ist als bei den vorhigen Disjunktionen, wird der Wert auf null gesetzt
                        literals[var]=0
                else:
                    literals[var]=1 if e>0 else -1 #falls es noch keinen Eintrag gibt wird einer mit entsprechendem Vorzeichen angelegt
        for var,sign in literals.items():#am Ende iterieren durch alle gefundenen Variablen
            if sign!=0: #0 bedeutet var kam mit unterschiedlichen Vorzeichen vor
                return int(var)*sign #zurückkonvertieren in int mit entsprechendem Vorzeichen
        return None #None falls nichts gefunden wurde
                        
    #gibt zurück ob das Problem bereits gelöst wurde
    def is_solved(self):
        return len(self.disjunctions)==0 #falls alle Disjunktionen erfüllt und somit aus der Liste entfernt wurden diese Konjunktion erfüllt
    
    #legt die Repräsentation dieser Klasse als String fest
    def __repr__(self):
        return str(self.disjunctions)

#parst einen cnf-String
def parse(input):
    lines=re.split("\n",input) #aufteilen in Zeilen
    start=0#Zeile, die den Header enthält
    while lines[start]=='' or lines[start][0]=='c':#Kommentare, beginnend mit 'c' werden übersprungen 
        logging.debug("skiping comment in line %i",start)
        start+=1
    segments=re.split(' |\t',lines[start])#Zeile aufteilen nach Whitespaces
    if segments[0]!='p' or segments[1]!='cnf': #der Header muss mit "p cnf" beginnen, sonst bricht der Parser hier ab
        logging.error('Header could not be read: %s',lines[start])
        return None
    # die nächsten beiden Elemente sind:
    num_vars=segments[2]    # die Anzahl der Variablen und
    num_disjunc=segments[3] # die Anzahl der Disjunktionen 
    disjunctions=[]#diese Liste wird alle Disjunktionen enthalten 
    current=[]#enthält die Variablen der Disjunktion die gerade gelesen wird
    for l in range(start+1,len(lines)):#alle zeilen durchgehen
        segments=re.split(' |\t',lines[l])#Zeile aufteilen nach Whitespaces
        for e in segments:
            if e=='':#leere elemente ignorieren 
                continue
            elif e=='c':#wenn die Zeile ein Kommentar ist oder enthält, mit der nächsten Zeile weitermachen
                logging.debug("skiping comment in line %i",l)
                break
            elif e=='0':#'0' terminiert ein Disjunktion
                if len(current)>0:#keine leeren Disjunktionen
                    disjunctions.append(Disjunction(current))#Disjunktions-Objekt wird erstellt und der Liste an Disjunktionen angehängt
                    current=[]
                break #der Rest der Zeile wird ignoriert
            else:
                try: #in allen anderen Fällen wird das Element zum Integer konvertiert und der Liste an Elementen angehängt
                    current.append(int(e))
                except:#falls die Konvertierung fehlschlägt wird die Zeile ignoriert
                    logging.warn("variable could not be read: %s in line %i",e,l+1)
                    break
    logging.debug("parsed problem: %s",disjunctions)
    return Problem(disjunctions)#am Ende alle Disjunktionen in einem Problem vereinen

class Solver():

    def __init__(self):
        pass



    # gibt eine Belegung, unter der das Problem p wahr wird, zuück oder None falls eine solche Belegung nicht existiert
    def solve(self,p): 
        try: #try außerhalb der Schleife sorgt dafür, dass beim Auftreten einer UnsolvableException nicht weiter versucht wird das Problem zu lösen
            while not p.is_solved() :
                logging.debug("remaining Problem: %s",p)
                unit=p.get_unit()#Unit-Propagtion versuchen 
                if unit!=None:#falls eine geeignte Variable gefunden, Variable setzen
                    logging.debug('Unit-Propagation, seting: %i',unit)
                    p.set(unit)
                    continue #zuürck an den Schleifenanfang um zu überprüfen, ob das Problem gelöst wurde

                literal=p.get_literal()#Pure-Literal versuchen 
                if literal!=None:
                    logging.debug('Pur Literal, seting: %i',literal)
                    p.set(literal)
                    continue

                var,backtrack=p.split()#split, falls keine Unit-Propagtion oder Pure-Literal möglich waren
                logging.debug('split, seting: %i',var)
                try:#nested try, denn wenn hier direkt eine UnsolvableException auftritt, können wir immer noch den anderen Weg versuchen
                    p.set(var)#setzen der Spilt-Variable 

                except UnsolvableException: #falls eine UnsolvableException auftritt, wenn die Splitvariable gesetzt wird, kann direkt ein Backtrack gemacht werden
                    logging.debug('backtracking problem to: %s',backtrack)
                    logging.debug('backtrack, seting: %i',-var)
                    p=backtrack #dazu wird p auf backtrack gesetzt..
                    p.set(-var) #..und die Split-Variable mit umgekehrten Vorzeichen gesetzt
                    continue    #Danach wird die Schleife weiter ausgeführt

                interpretation=self.solve(p)
                if interpretation != None:
                    return interpretation
                else:
                    logging.debug('backtracking problem to: %s',backtrack)
                    logging.debug('backtrack, seting: %i',-var)
                    p=backtrack 
                    p.set(-var) 
                    

        except UnsolvableException: #Falls das Problem durch setzen einer Variable unlösbar geworden ist,..
            return None             #..wird None zurückgegeben
        return p.interpretation


if __name__=='__main__':
    logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s', level=logging.ERROR)
    if len(sys.argv)>=2:
        file=sys.argv[1]
    else:
        file='test1.cnf'

    
    with open(file) as f:
        problem=parse(f.read())
        #print('problem: ',problem)
        solution=Solver().solve(problem)
        logging.debug("solution: %s",solution)
        #print('solution: ',solution)
        if solution!=None:
            print('% SZS status Satisfiable')
        else:
            print('% SZS status Unsatisfiable')
