import os,logging,sys
import SAT_Solver
from tqdm import tqdm

logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s', level=logging.ERROR)

unsatisfied=0
satisfied=0

solver=SAT_Solver.Solver()
for path in tqdm(os.listdir(sys.argv[1])):
    with open(os.path.join(sys.argv[1],path),"r") as f: 
        p=SAT_Solver.parse(f.read())
        s=solver.solve(p)
        if s ==None:
            unsatisfied+=1
        else :
            satisfied+=1
print("satisfied: ",satisfied)
print("unsatisfied: ",unsatisfied)