import os,logging,sys
from tqdm import tqdm
import subprocess

logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s', level=logging.ERROR)

unsatisfied=0
satisfied=0

for path in tqdm(os.listdir(sys.argv[1])):
    with subprocess.Popen(["G:\\cpp\\SAT_Solver\\x64\\Debug\\SAT Solver.exe",os.path.join(sys.argv[1],path)], stdout=subprocess.PIPE) as proc:
        #print(os.path.join(sys.argv[1],path))
        s=proc.communicate()[0]
        #print(s)
    if s ==b'% SZS status Satisfiable\r\n':
        satisfied+=1
    else :
        unsatisfied+=1
print("satisfied: ",satisfied)
print("unsatisfied: ",unsatisfied)