import sys
#size=sys.getrecursionlimit()+1
size=1100
comment="""

c Dieses Problem ist so angelegt, dass es nötig ist viele Splits zu machen 
c und dass bei einer ungünstigen naiven Wahl der Variable, die dann gesetzt werden soll, """+str(size)+""" Splits erforderlich sind.
c Eine Python-Implementierung, die Splits mittels Rekursion umsetzt, würde so die maximale Rekursionstiefe überschreiten,
c falls sie überhaupt in der gegebenen Zeit soweit kommt.


c Bei einer günstigen Wahl kann eine Lösung nach nur ein paar Schritten gefunden werden.


"""

with open('problem.cnf','w',encoding='UTF-8') as f:
    f.write(comment)
    f.write('p cnf '+ str(size) +' ' +str(size)+ '\n')

    for clause in range(1,size):
        for var in range(1,size+1):
            if(var%50==0):
                f.write('\n')
            if (var<clause):
                var=-var
            f.write(str(var)+' ')
        f.write(' 0\n')
    """
    for clause in range(1,size):
        for var in range(1,size+1):
            if(var%50==0):
                f.write('\n')
            if (var!=clause):
                var=-var
            f.write(str(var)+' ')
        f.write(' 0\n')
    """ 
    for var in range(1,size):
        f.write(str(-var)+' ')
    f.write(str(size)+' ')
    f.write('0\n')
    for var in range(1,size+1):
        f.write(str(-var)+' ')
    f.write('0\n')
    f.close