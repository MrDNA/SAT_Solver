##𓌷𓂝𓏏𓆄𓇩

SAT Solver der Gruppe 1
name: "mAat"
pretty-name: "𓌷𓂝𓏏𓆄𓇩"


##Python

Erfordert nur Python3-Umgebung (getestet mit Python 3.7)
Zum Ausführen:
    python3 SAT_Solver.py INPUT_FILE

##C++

Zum Kompilieren:
    g++ SAT_Solver.cpp -o mAat
und Ausführen mit:
    mAat INPUT_FILE
