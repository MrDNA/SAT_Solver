// SAT_Solver.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>

class Problem;

class Disjunction {
	friend class Problem;
private:
	int* elements;
	int size, actual_size;
public:
	Disjunction(int* e,int len): elements(e),size(len),actual_size(len) {}
	Disjunction(const Disjunction& d);
	~Disjunction() { delete[] elements; }
	int get_unit();
	int set(int x);
	bool solvable();
	int get_element_for_split();
};

class Problem {
	Disjunction** disjunctions;
	int size,actual_size;
	int num_vars;
public:
	Problem(Disjunction** dis, int len, int num_vars) :disjunctions(dis), size(len),actual_size(len), num_vars(num_vars) {};
	Problem(const Problem& p);
	~Problem();
	void set(int x);
	int split();
	int get_unit();
	int get_literal();
	bool is_solved();
};


Disjunction::Disjunction(const Disjunction& d) {
	size = d.size;
	actual_size = d.actual_size;
	elements = new int[size];
	for (int i = 0; i < size; i++) {
		elements[i] = d.elements[i];
	}
}

int Disjunction::get_unit() {
	int unit=0;
	if (actual_size != 1) {
		return 0;
	}
	for (int i = 0; i < size; i++) {
		if (elements[i] != 0 && unit == 0){
			unit = elements[i];
		}
		else if (elements[i] != 0 && unit != 0) {
			return 0;
		}
	}
	return unit;
}

int Disjunction::set(int x) {
	int found = -1;
	for (int i = 0; i < size; i++) {
		if (abs(elements[i]) == abs(x)) {
			found = i;
			break;
		}
	}
	if (found == -1) {
		return 0;
	}
	else if (elements[found] == x) {
		return 1;
	}
	else {
		elements[found] = 0;
		actual_size--;
		return -1;
	}

}

bool Disjunction::solvable() {
	if (actual_size > 0) return true;
	/*for (int i = 0; i < size; i++) {
		if (elements[i] != 0) return true;
	}*/
	return false;
}

int Disjunction::get_element_for_split() {
	for (int i = 0; i < size; i++) {
		if (elements[i] != 0) return elements[i];
	}
	return 0;
}

Problem::Problem(const Problem& p) {
	size = p.size;
	actual_size = p.actual_size;
	num_vars = p.num_vars;
	disjunctions = new Disjunction*[size];
	for (int i = 0; i < size; i++) {
		if (p.disjunctions[i] != nullptr) {
			Disjunction* temp = new Disjunction(*p.disjunctions[i]);
			disjunctions[i] = temp;
		}
		else {
			disjunctions[i] = nullptr;
		}
	}
}

Problem::~Problem() {
	for (int i = 0; i < size; i++) {
		//delete[] disjunctions[i]->elements;
		delete disjunctions[i];
	}
	delete[] disjunctions;
}

void Problem::set(int x) {
	for (int i = 0; i < size; i++) {
		if (disjunctions[i] != nullptr) {
			int r = disjunctions[i]->set(x);
			if (r == 1) {
				delete disjunctions[i];
				disjunctions[i] = nullptr;
				actual_size--;
			}
			else if (!disjunctions[i]->solvable()) {
				throw 0;
			}
		}
	}
}

int Problem::split() {
	int* vars = new int[num_vars]();
	for (int i = 0; i < size; i++) {
		if (disjunctions[i] != nullptr) {
			for (int j = 0; j < disjunctions[i]->size; j++) {
				int var=disjunctions[i]->elements[j];
				if (var != 0) {
					vars[abs(var) - 1] += (var > 0) ? 1 : -1;
				}
			}
		}
	}
	int max = 0, index = -1;
	for (int i = 0; i < num_vars; i++) {
		if (abs(vars[i]) > abs(max)) {
			index = i;
			max = vars[i];
		}
	}
	if (index >= 0) {
		return max < 0 ? -(index + 1) : index + 1;
	}
	for (int i = 0; i < size; i++) {
		if (disjunctions[i] != nullptr) {
			return disjunctions[i]->get_element_for_split();
		}
	}
	return 0;
}

int Problem::get_unit() {
	for (int i = 0; i < size; i++) {
		if (disjunctions[i] != nullptr) {
			int unit = disjunctions[i]->get_unit();
			if (unit!=0) {
				return unit;
			}
		}
	}
	return 0;
}

int Problem::get_literal() {
	if (actual_size == 1) {
		for (int i = 0; i < size; i++) {
			if (disjunctions[i] != nullptr) {
				return disjunctions[i]->get_element_for_split();
			}
		}
	}
	int* literals = new int[num_vars]();
	for (int i = 0; i < size; i++) {
		if (disjunctions[i] != nullptr) {
			for (int j = 0; j < disjunctions[i]->size; j++) {
				int var = disjunctions[i]->elements[j];
				if (var != 0) {
					if ((literals[abs(var)-1] == 1 && var < 0) || (literals[abs(var)-1] == -1 && var > 0)) {
						literals[abs(var)-1] = -2;
					}
					else if(literals[abs(var) - 1] == 0){
						literals[abs(var)-1] = var < 0 ? -1 : 1;
					}
				}
			}
		}
	}
	int r = 0;
	for (int i = 0; i < num_vars; i++) {
		if (literals[i] != 0 && literals[i] != -2) {
			r = (i+1) * literals[i];
			break;
		}
	}
	delete[] literals;
	return r;
}

bool Problem::is_solved() {
	if (actual_size > 0) return false;
	/*for (int i = 0; i < size; i++) {
		if (disjunctions[i] != nullptr) {
			return false;
		}
	}*/
	return true;
}

Problem * parse(std::ifstream* file) {
	if (file->is_open()) {
		int num_vars=0;
		int num_clauses=0;
		std::string line;
		while (std::getline(*file, line)) {
			if (line[0] == 'c'|| line.size() == 0) {
				continue;
			}
			else if (line[0] == 'p') {
				//header
				if (line.compare(2, 3, "cnf") == 0) {
					line.erase(0, 6);
					while (line[0] == ' ')
					{
						line.erase(0, 1);
					}
					size_t pos = line.find(' ');
					if (pos == std::string::npos) {
						pos = line.find('\t');
					}
					std::string vars = line.substr(0, pos + 1);
					line.erase(0, pos+1);
					while (line[0]==' ')
					{
						line.erase(0, 1);
					}
					pos = line.find(' ');
					if (pos == std::string::npos) {
						pos = line.find('\t');
					}
					pos = (pos == std::string::npos) ? line.size() : pos;
					std::string clauses = line.substr(0, pos + 1);
					num_vars = std::stoi(vars);
					num_clauses = std::stoi(clauses);
					break;
				}
				else throw - 1;
			}
			else throw - 2;
		}
		Disjunction** dis = new Disjunction * [num_clauses]();
		int dis_counter = 0;
		int* current = new int[num_vars]();
		int counter = 0;
		while (std::getline(*file, line)) {
			if (line[0] == 'c') {
				continue;
			}
			size_t pos;
			while ((pos = line.find(' ')) != std::string::npos|| (pos = line.find('\t')) != std::string::npos) {
				try {
					int i = std::stoi(line.substr(0, pos));
					if (i != 0) {
						current[counter++] = i;
					}
					else if (counter>0){
						Disjunction* d = new Disjunction(current,counter);
						dis[dis_counter++] = d;
						current = new int[num_vars]();
						counter = 0;
					}
				}
				catch (...) {
					;
				}
				line.erase(0, pos + 1);
			}
			try {
				int i = std::stoi(line);
				if (i != 0) {
					current[counter++] = i;
				}
				else if (counter > 0) {
					Disjunction* d = new Disjunction(current, counter);
					dis[dis_counter++] = d;
					current = new int[num_vars]();
					counter = 0;
				}
			}
			catch (...) {
				;
			}

		}
		return new Problem(dis, dis_counter, num_vars);
	}
	throw - 3;
}

int solve(Problem** p) {
	try {
		while (!(*p)->is_solved()) {
			int var = (*p)->get_unit();
			if (var != 0) {
				//std::cout << "Unit: "<<var <<"\n";
				(*p)->set(var);
				continue;
			}
			var = (*p)->get_literal();
			if (var != 0) {
				//std::cout << "Literal: " << var << "\n";
				(*p)->set(var);
				continue;
			}
			var= (*p)->split();
			if (var == 0) {
				throw var;
			}
			Problem * backtrack=new Problem (**p);
			try {
				//std::cout << "Split: " << var << "\n";
				(*p)->set(var);
				int sol=solve(p);
				if (sol != 0) {
					delete backtrack;
					return sol;
				}
				throw sol;
			}
			catch (...){
				//std::cout << "Backtrack: " << -var << "\n";
				delete *p;
				(*p) = backtrack;
				(*p)->set(-var);
			}
		}
	}
	catch (...) {
		//std::cout << "excep\n";
		return 0;
	}
	//std::cout << "solved " << &p << "\n";
	return (*p)->is_solved() ? 1 : 0;
}


int main(int argc, char** argv)
{
	
	std::string filename="G:\\Google Drive\\Uni\\KI\\SAT Solver\\competition-test\\problems\\Unsat6.cnf"; //noch zu testen
	//std::string filename = "G:\\cnf\\uf50-02.cnf";
	//std::string filename = "G:\\cpp\\SAT_Solver\\python\\problem.cnf";

	if (argc > 1) {
		filename = argv[1];
	}
	else {
		std::cerr << "no filename provided\n";
	}
	Problem* p;
	std::ifstream* file= new std::ifstream (filename);
	p = parse(file);
	file->close();
	delete file;
	if (p == nullptr) {
		std::cerr << "could not parse " << filename << "\n";
		return EXIT_FAILURE;
	}
	int sol = solve(&p);
	delete p;
    std::cout << ((sol==1)?"% SZS status Satisfiable":"% SZS status Unsatisfiable")<<"\n";
	return EXIT_SUCCESS;
}
